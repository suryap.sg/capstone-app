// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SG_AUTHENTICATION_API: 'https://dev-auth.senecaglobal.in',
  SG_AUTH_API: 'https://dev-auth.senecaglobal.in', // endpoint for authenticate and authorize an associate
  SG_CLIENT_ID: 'c18d900d-041c-4f0b-b79d-87aabe03cfc4'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
