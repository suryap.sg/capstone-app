import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MsalGuard } from '@azure/msal-angular';
import { BrowserUtils } from '@azure/msal-browser';
import { AccountStatusComponent } from './core/components/account-status/account-status.component';
import { HomeComponent } from './core/components/home/home.component';
import { LoginFailedComponent } from './core/components/login-failed/login-failed.component';
import { SGTokenGuard } from './core/guards/sg-token.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: AccountStatusComponent
      },
      {
        path: 'training',
        loadChildren: () => import('./training/training.module').then(module => module.TrainingModule),
        canActivate: [SGTokenGuard]
      },
      {
        path: 'cdp-by-certs',
        loadChildren: () =>
          import('./certifications/certifications.module').then(module => module.CertificationsModule),
        canActivate: [SGTokenGuard]
      }
    ],
    canActivate: [MsalGuard]
  },
  {
    path: 'login-failed',
    component: LoginFailedComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // Don't perform initial navigation in iframes or popups
      initialNavigation: !BrowserUtils.isInIframe() && !BrowserUtils.isInPopup() ? 'enabled' : 'disabled'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
