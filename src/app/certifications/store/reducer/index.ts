import { ActionReducer, createReducer, MetaReducer, on } from '@ngrx/store';
import { CertificationModel } from 'src/app/models/certification-model';
import { environment } from 'src/environments/environment';
import * as fromCertActions from '../actions/certification.actions';

export const certificationFeatureKey = 'certification';

export interface State {
  certifications: CertificationModel[] | undefined;
  loadingCertifications: boolean;
  loadingCertificationsFailed: boolean;
}

const initialState: State = {
  certifications: undefined,
  loadingCertifications: false,
  loadingCertificationsFailed: false
};

export const reducers: ActionReducer<State> = createReducer<State>(
  initialState,
  on(fromCertActions.loadCertifications, (state, action) => {
    return {
      ...state,
      loadingCertifications: true,
      loadingCertificationsFailed: false
    };
  }),
  on(fromCertActions.loadCertificationsSuccess, (state, { data }) => {
    return {
      ...state,
      certifications: data,
      loadingCertifications: false,
      loadingCertificationsFailed: false
    };
  }),
  on(fromCertActions.loadCertificationsFailure, (state, _) => {
    return {
      ...state,
      loadingCertifications: false,
      loadingCertificationsFailed: true
    };
  }),
  on(fromCertActions.fetchCertificationSuccess, (state, { data }) => {
    return {
      ...state,
      certifications: [...(state.certifications || []), data]
    };
  })
);

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
