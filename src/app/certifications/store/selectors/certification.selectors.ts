import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CertificationModel } from 'src/app/models/certification-model';
import * as fromCertsReducer from '../reducer';

type CertState = fromCertsReducer.State;
const getCertState = (state: any) => state[fromCertsReducer.certificationFeatureKey];

export const selectLoadingCertifications = createSelector(
  getCertState,
  (state: CertState) => state.loadingCertifications
);

export const selectCertifications = createSelector(getCertState, (state: CertState) => state.certifications);

export const selectFilteredCertifications = (searchTerm: string) =>
  createSelector(selectCertifications, (certifications: CertificationModel[] | undefined) => {
    if (!searchTerm) {
      return certifications;
    }

    return certifications?.filter(cert => cert.name.toLowerCase().includes(searchTerm.toLowerCase()));
  });

export const selectCertification = (id: string) =>
  createSelector(selectCertifications, (certifications: CertificationModel[] | undefined) => {
    if (!id) {
      return null;
    }

    return certifications?.find(cert => cert.id === id);
  });
