import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { CertificationModel } from 'src/app/models/certification-model';
import { CertificationsService } from '../../services/certifications.service';
import * as fromCertActions from '../actions/certification.actions';

@Injectable()
export class CertificationEffects {
  loadCertifications$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(fromCertActions.loadCertifications),
      mergeMap(() =>
        this.certService.listCertifications().pipe(
          map(response => {
            return fromCertActions.loadCertificationsSuccess({ data: response });
          }),
          catchError(error => of(fromCertActions.loadCertificationsFailure({ error })))
        )
      )
    );
  });

  fetchCertification$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(fromCertActions.fetchCertification),
      mergeMap(({ id }) => {
        return this.certService.getCertification(id).pipe(
          map(response => {
            return fromCertActions.fetchCertificationSuccess({ data: response as CertificationModel });
          }),
          catchError(error => of(fromCertActions.fetchCertificationFailure({ error })))
        );
      })
    );
  });

  constructor(private actions$: Actions, private readonly certService: CertificationsService) {}
}
