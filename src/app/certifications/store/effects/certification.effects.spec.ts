import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CertificationEffects } from './certification.effects';

describe('CertificationEffects', () => {
  let actions$: Observable<any>;
  let effects: CertificationEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CertificationEffects, provideMockActions(() => actions$)]
    });

    effects = TestBed.inject(CertificationEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
