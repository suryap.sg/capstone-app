import * as fromCertification from './certification.actions';

describe('loadCertifications', () => {
  it('should return an action', () => {
    expect(fromCertification.loadCertifications().type).toBe('[Certification] Load Certifications');
  });
});
