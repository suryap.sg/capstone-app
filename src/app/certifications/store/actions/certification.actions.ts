import { createAction, props } from '@ngrx/store';
import { CertificationModel } from 'src/app/models/certification-model';

export const loadCertifications = createAction('[Certification List Page] Load Certifications');

export const loadCertificationsSuccess = createAction(
  '[Certification] Load Certifications Success',
  props<{ data: any }>()
);

export const loadCertificationsFailure = createAction(
  '[Certification] Load Certifications Failure',
  props<{ error: any }>()
);

export const fetchCertification = createAction('[Certification Detail] Fetch Certification', props<{ id: string }>());

export const fetchCertificationSuccess = createAction(
  '[Certification Effect] Fetch Certification Success',
  props<{ data: CertificationModel }>()
);

export const fetchCertificationFailure = createAction(
  '[Certification Effect] Fetch Certification Failure',
  props<{ error: any }>()
);

export const saveCertification = createAction(
  '[Certification Detail] Save Certification',
  props<{ item: CertificationModel }>()
);
export const saveCertificationSuccess = createAction(
  '[Certification Effect] Save Certification Success',
  props<{ item: CertificationModel }>()
);
export const saveCertificationFailure = createAction(
  '[Certification Effect] Save Certification Failure',
  props<{ error: any }>()
);
