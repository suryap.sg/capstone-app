import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { CertificationModel } from 'src/app/models/certification-model';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class CertificationsService {
  constructor() {}

  listCertifications(): Observable<CertificationModel[]> {
    const itemsStore = localStorage.getItem('certifications');
    let items = [];
    if (itemsStore) {
      items = JSON.parse(itemsStore);
    }

    return of(items).pipe(delay(2000));
  }

  getCertification(id: string): Observable<CertificationModel | undefined> {
    return this.listCertifications().pipe(
      map(certifications => certifications.find(certification => certification.id === id))
    );
  }

  saveCertification(certification: CertificationModel): Observable<CertificationModel> {
    certification.id = uuidv4();
    certification.updated = new Date();

    this.saveToLocalStorage('certifications', certification);
    return of(certification);
  }

  updateCertification(certification: CertificationModel): Observable<CertificationModel> {
    certification.updated = new Date();
    this.saveToLocalStorage('certifications', certification);
    return of(certification);
  }

  saveToLocalStorage(key: string, incoming: any) {
    const itemsStore = localStorage.getItem(key);
    let items = [];
    if (itemsStore) {
      items = JSON.parse(itemsStore);
    }

    const existingItemIndex = items.findIndex((item: any) => item.id === incoming.id);

    if (existingItemIndex < 0) {
      items.push(incoming);
    } else {
      items[existingItemIndex] = incoming;
    }

    localStorage.setItem(key, JSON.stringify(items));

    return items;
  }
}
