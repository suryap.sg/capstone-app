import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import { CertificationModel } from 'src/app/models/certification-model';
import * as fromCertActions from '../../../store/actions/certification.actions';
import * as fromCertSelector from '../../../store/selectors/certification.selectors';

@Component({
  selector: 'app-add-edit-cd-by-cert',
  templateUrl: './add-edit-cd-by-cert.component.html',
  styleUrls: ['./add-edit-cd-by-cert.component.scss']
})
export class AddEditCdByCertComponent implements OnInit, OnDestroy {
  destroy$ = new Subject();
  newLearningCertificateForm!: FormGroup;

  @Input() associateCertification!: { id: string; name: string };
  saving = false;

  get nameControl() {
    return this.newLearningCertificateForm.get('name');
  }

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly activateRoute: ActivatedRoute,
    private readonly store: Store
  ) {}

  ngOnInit(): void {
    this.buildCDByCertsForm();
    this.readCertificationId();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  buildCDByCertsForm() {
    this.newLearningCertificateForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
      description: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
      price: this.formBuilder.control('', [Validators.required, Validators.minLength(0)]),
      approver: this.formBuilder.control(null, [Validators.required])
    });
  }

  readCertificationId() {
    this.activateRoute.params
      .pipe(
        switchMap(({ id }) => {
          // dispatch an action to get certification
          this.store.dispatch(fromCertActions.fetchCertification({ id }));

          // select the certification from store
          return this.store.select(fromCertSelector.selectCertification(id));
        }),
        filter(cert => !!cert),
        tap(response => this.applyCertificationToForm(response as CertificationModel)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  handleSave() {
    if (!this.newLearningCertificateForm.valid) {
      return;
    }

    const { value } = this.newLearningCertificateForm;

    if (this.associateCertification?.id) {
      this.updateCertification({
        id: this.associateCertification.id,
        ...value
      });
    } else {
      this.addCertification(value);
    }
  }

  addCertification(incomingCertification: CertificationModel) {
    this.store.dispatch(fromCertActions.saveCertification({ item: incomingCertification }));
  }

  updateCertification(incomingCertification: CertificationModel) {
    this.store.dispatch(fromCertActions.saveCertification({ item: incomingCertification }));
  }

  navigateToList() {
    this.router.navigateByUrl('/cdp-by-certs/list');
  }

  applyCertificationToForm(certification: CertificationModel | undefined) {
    if (!certification) {
      return;
    }

    this.newLearningCertificateForm.patchValue(certification);
  }
}
