import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCdByCertComponent } from './add-edit-cd-by-cert.component';

describe('AddEditCdByCertComponent', () => {
  let component: AddEditCdByCertComponent;
  let fixture: ComponentFixture<AddEditCdByCertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddEditCdByCertComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditCdByCertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
