import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertsVisualComponent } from './certs-visual.component';

describe('CertsVisualComponent', () => {
  let component: CertsVisualComponent;
  let fixture: ComponentFixture<CertsVisualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CertsVisualComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertsVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
