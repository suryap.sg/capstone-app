import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CertificationModel } from 'src/app/models/certification-model';

@Component({
  selector: 'app-certification-item',
  templateUrl: './certification-item.component.html',
  styleUrls: ['./certification-item.component.scss']
})
export class CertificationItemComponent {
  @Input() public certification!: CertificationModel;
  @Output() public manage = new EventEmitter();
  constructor() {}

  ngOnDestroy(): void {
    this.manage.complete();
  }

  manageCertification() {
    this.manage.emit();
  }
}
