import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import * as fromCertReducer from '../../../store/reducer';
import * as fromCertReducerActions from '../../../store/actions/certification.actions';
import * as fromCertSelector from '../../../store/selectors/certification.selectors';
import { map, mergeMap, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-list-certifications',
  templateUrl: './list-certifications.component.html',
  styleUrls: ['./list-certifications.component.scss']
})
export class ListCertificationsComponent implements OnInit {
  destroy$ = new Subject();
  filterControl = new FormControl();
  loadingCertifications$ = this.store.select(fromCertSelector.selectLoadingCertifications);
  associateCertifications$ = this.filterControl.valueChanges.pipe(
    startWith(''),
    mergeMap((value: string) => this.store.select(fromCertSelector.selectFilteredCertifications(value))),
    map(result => result || [])
  );
  constructor(private readonly router: Router, private readonly store: Store<fromCertReducer.State>) {}

  ngOnInit(): void {
    this.store.dispatch(fromCertReducerActions.loadCertifications());
  }

  newCertification() {
    this.router.navigateByUrl('/cdp-by-certs/new');
  }

  manageCertification(id: string) {
    this.router.navigateByUrl(`/cdp-by-certs/manage/${id}`);
  }
}
