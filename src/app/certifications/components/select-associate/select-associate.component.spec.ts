import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectAssociateComponent } from './select-associate.component';

describe('SelectAssociateComponent', () => {
  let component: SelectAssociateComponent;
  let fixture: ComponentFixture<SelectAssociateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SelectAssociateComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectAssociateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
