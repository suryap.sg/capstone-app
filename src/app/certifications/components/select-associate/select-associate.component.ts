import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { of } from 'rxjs';
import { catchError, debounceTime, filter, map, switchMap, tap } from 'rxjs/operators';
import { AccountService, AssociateModel } from 'src/app/core/account.service';

@Component({
  selector: 'app-select-associate',
  templateUrl: './select-associate.component.html',
  styleUrls: ['./select-associate.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectAssociateComponent),
      multi: true
    }
  ]
})
export class SelectAssociateComponent implements ControlValueAccessor {
  registerOnChangeFn: any;
  registerOnTouchedFn: any;

  associateFormControl = new FormControl();
  filteredUsers$ = this.associateFormControl.valueChanges.pipe(
    debounceTime(500),
    filter(value => value && value.length > 2),
    tap(() => {
      this.failedToSearch = false;
      this.searchingUsers = true;
    }),
    switchMap(value =>
      this.accountService.searchAssociates(value).pipe(
        catchError(() => {
          this.failedToSearch = true;
          return of({
            data: []
          });
        }),
        map(response => response.data)
      )
    ),
    tap(() => {
      this.searchingUsers = false;
    })
  );
  selectedUsers: Array<AssociateModel> = [];
  searchingUsers = false;
  failedToSearch = false;
  noUsersMatched = false;
  assigningRoleToUsers = false;

  constructor(private readonly accountService: AccountService) {}

  writeValue(users: AssociateModel[]): void {
    if (users?.length > 0) {
      this.selectedUsers.push(...users);
    }
  }

  registerOnChange(fn: any): void {
    this.registerOnChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.registerOnTouchedFn = fn;
  }

  selectUser(associate: any) {
    this.selectedUsers.push(associate);
    this.registerOnChangeFn(this.selectedUsers);
    this.associateFormControl.reset();
  }

  removeUser(user: any) {
    this.selectedUsers = this.selectedUsers.filter(u => user !== u);
    this.registerOnChangeFn(this.selectedUsers);
  }

  public userDisplayName = (user: AssociateModel) => (user ? user.full_name : '');
}
