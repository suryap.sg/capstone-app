import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertsVisualComponent } from './components/certs-visual/certs-visual.component';
import { RouterModule } from '@angular/router';
import { AddEditCdByCertComponent } from './components/certs-visual/add-edit-cd-by-cert/add-edit-cd-by-cert.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import { SelectAssociateComponent } from './components/select-associate/select-associate.component';
import { ListCertificationsComponent } from './components/certs-visual/list-certifications/list-certifications.component';
import { CertificationItemComponent } from './components/certs-visual/certification-item/certification-item.component';
import { StoreModule } from '@ngrx/store';
import * as fromCertification from './store/reducer';
import { EffectsModule } from '@ngrx/effects';
import { CertificationEffects } from './store/effects/certification.effects';

@NgModule({
  declarations: [
    CertsVisualComponent,
    AddEditCdByCertComponent,
    SelectAssociateComponent,
    ListCertificationsComponent,
    CertificationItemComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CertsVisualComponent,
        children: [
          { path: '', redirectTo: 'new', pathMatch: 'full' },
          { path: 'new', component: AddEditCdByCertComponent },
          { path: 'manage/:id', component: AddEditCdByCertComponent },
          { path: 'list', component: ListCertificationsComponent }
        ]
      }
    ]),
    StoreModule.forFeature(fromCertification.certificationFeatureKey, fromCertification.reducers, {
      metaReducers: fromCertification.metaReducers
    }),
    EffectsModule.forFeature([CertificationEffects])
  ]
})
export class CertificationsModule {}
