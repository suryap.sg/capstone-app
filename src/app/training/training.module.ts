import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TrainingVisualComponent } from './components/training-visual/training-visual.component';

const routes: Routes = [
  {
    path: '',
    component: TrainingVisualComponent
  }
];

@NgModule({
  declarations: [TrainingVisualComponent],
  imports: [CommonModule, RouterModule.forChild(routes)]
})
export class TrainingModule {}
