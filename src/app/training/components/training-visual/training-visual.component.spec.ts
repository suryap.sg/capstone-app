import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingVisualComponent } from './training-visual.component';

describe('TrainingVisualComponent', () => {
  let component: TrainingVisualComponent;
  let fixture: ComponentFixture<TrainingVisualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingVisualComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
