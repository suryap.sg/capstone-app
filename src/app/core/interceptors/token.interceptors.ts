import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AccountService } from '../account.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {
  constructor(private router: Router, private readonly accountService: AccountService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // if graph end point do not act
    if (request.url.includes('graph.microsoft.com') || request.url.includes('get_access_token_by_ms_token')) {
      return next.handle(request);
    }

    const tokenResponse = this.accountService.sgTokenResponse;

    if (!tokenResponse) {
      return throwError(new Error('Authorization error: token expired'));
    }

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${tokenResponse.access_token}`
      }
    });

    return next.handle(request).pipe(
      tap(
        event => {},
        error => {
          if (error instanceof HttpErrorResponse) {
            switch (error.status) {
              case 401:
                this.router.navigate(['']);
                break;
              // track 500 responses messages
              case 500:
                break;
              default:
                // No default action
                break;
            }
          }
        }
      )
    );
  }
}
