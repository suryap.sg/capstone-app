import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { MS_GRAPH_ENDPOINT } from '../msal-config';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, EMPTY, Observable, of, Subject } from 'rxjs';

export const CAPSTONE_SG_TOKEN_RESPONSE_IDENTIFIER = 'capstone_sg_toke_response';

export enum LoginProgressStatus {
  MSAL_AUTHENTICATING = 'MSAL_AUTHENTICATING',
  SG_AUTHENTICATING = 'SG_AUTHENTICATING',
  SG_AUTHENTICATION_FAILED = 'SG_AUTHENTICATION_FAILED',
  AUTHENTICATED = 'AUTHENTICATED'
}

export type AccountModel = {
  displayName?: string;
  mail?: string;
  givenName?: string;
  surname?: string;
  userPrincipalName?: string;
  id?: string;
  profilePictureUrl?: string;
};

export interface UserScopeModel {
  access_level: string;
  associate_id: string;
  associate_name: string;
  client_id: string;
  client_name: string;
  client_scope_id: string;
  email: string;
  is_associate_active: boolean;
  is_client_active: boolean;
  is_client_scope_active: boolean;
  is_role_active: boolean;
  is_user_scope_active: boolean;
  role_id: string;
  role_name: string;
  user_scope_id: string;
}

type ResponseModel<T> = { data: T[]; status: string };
export type AssociateModel = {
  email: string;
  full_name: string;
  id: string;
};

type SGTokenResponseModel = {
  access_token: string;
  associate_id: string;
  client: string;
  full_name: string;
  refresh_token: string;
};

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  profile!: AccountModel;
  sgTokenResponse!: SGTokenResponseModel;
  userScopes!: UserScopeModel[];
  loginProgress$ = new BehaviorSubject<LoginProgressStatus>(LoginProgressStatus.MSAL_AUTHENTICATING);
  private readonly env = environment;

  public get associateID(): string {
    return this.sgTokenResponse?.associate_id;
  }

  constructor(private readonly http: HttpClient, private readonly msalService: MsalService) {
    this.checkForSGToken();
  }

  getAccount() {
    this.loginProgress$.next(LoginProgressStatus.MSAL_AUTHENTICATING);
    return this.http.get(MS_GRAPH_ENDPOINT).pipe(
      tap(profile => (this.profile = profile)),
      mergeMap(() => this.getSGAccessToken()),
      mergeMap(() =>
        this.getProfilePicture().pipe(
          tap(profilePictureUrl => {
            this.profile.profilePictureUrl = profilePictureUrl;
          })
        )
      ),
      mergeMap(() =>
        this.listUserScopes().pipe(
          tap(scopes => {
            this.userScopes = scopes;
          })
        )
      ),
      map(() => this.profile),
      catchError(_ => {
        this.loginProgress$.next(LoginProgressStatus.SG_AUTHENTICATION_FAILED);
        return EMPTY;
      })
    );
  }

  getMsalToken() {
    return this.msalService.acquireTokenSilent({ scopes: ['user.read'] });
  }

  getProfilePicture() {
    return this.getMsalToken().pipe(
      mergeMap(tokenResponse =>
        this.http
          .get('https://graph.microsoft.com/v1.0/me/photos/48x48/$value', {
            headers: {
              Authorization: 'Bearer ' + tokenResponse.accessToken,
              'Content-Type': 'image/jpg'
            },
            responseType: 'blob'
          })
          .pipe(
            map((response: any) => {
              const urlCreator = window.URL || window.webkitURL;
              return urlCreator.createObjectURL(response);
            })
          )
      )
    );
  }

  getSGAccessToken() {
    const client_id = '971b07f8-dee6-4171-ac61-8b4e93243811';
    this.loginProgress$.next(LoginProgressStatus.SG_AUTHENTICATING);
    return this.getMsalToken().pipe(
      mergeMap(tokenResponse =>
        this.http
          .post(`${this.env.SG_AUTHENTICATION_API}/get_access_token_by_ms_token`, {
            client_id,
            ms_access_token: tokenResponse.accessToken
          })
          .pipe(
            catchError(error => of(null)),
            tap(response => this.processSGToken(response))
          )
      )
    );
  }

  /**
   * Refer: http://192.168.1.66:5010/swagger/#/User%20Scope%20APIs/post_api_user_scope_list_by_associate
   * @param client_id
   * @param associate_id
   * @returns
   */
  listUserScopes(): Observable<UserScopeModel[]> {
    return this.http
      .post<ResponseModel<UserScopeModel>>(`${environment.SG_AUTH_API}/api/user_scope/list_by_associate`, {
        client_id: environment.SG_CLIENT_ID,
        associate_id: this.associateID
      })
      .pipe(map(response => response.data.filter(item => item.associate_id === this.associateID)));
  }

  searchAssociates(searchTerm: string): Observable<ResponseModel<AssociateModel>> {
    return this.http.post<ResponseModel<AssociateModel>>(`${environment.SG_AUTH_API}/api/user/search`, {
      email: searchTerm
    });
  }

  private processSGToken(sgTokenResponse: any) {
    if (!sgTokenResponse) {
      this.loginProgress$.next(LoginProgressStatus.SG_AUTHENTICATION_FAILED);
      return;
    }

    this.loginProgress$.next(LoginProgressStatus.AUTHENTICATED);
    this.sgTokenResponse = sgTokenResponse.data;

    localStorage.setItem(CAPSTONE_SG_TOKEN_RESPONSE_IDENTIFIER, JSON.stringify(sgTokenResponse));
  }

  private checkForSGToken() {
    const tokenResponseRaw = localStorage.getItem(CAPSTONE_SG_TOKEN_RESPONSE_IDENTIFIER);

    if (!tokenResponseRaw) {
      return;
    }

    // there are good changes that localStorage can be manipulated and JSON.parse will fail parsing
    try {
      this.sgTokenResponse = JSON.parse(tokenResponseRaw);
      this.loginProgress$.next(LoginProgressStatus.AUTHENTICATED);
    } catch (error) {}
  }
}
