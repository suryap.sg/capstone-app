import { TestBed } from '@angular/core/testing';

import { SGTokenGuard } from './sg-token.guard';

describe('SGTokenGuard', () => {
  let guard: SGTokenGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SGTokenGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
