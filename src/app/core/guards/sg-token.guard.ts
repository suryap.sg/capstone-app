import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AccountService, LoginProgressStatus } from '../account.service';

@Injectable({
  providedIn: 'root'
})
export class SGTokenGuard implements CanActivate {
  constructor(private readonly accountService: AccountService, private readonly router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.accountService.loginProgress$.pipe(
      map((response: LoginProgressStatus) => {
        // still login was not successful - so redirect ot home
        if (response !== LoginProgressStatus.AUTHENTICATED) {
          this.router.navigateByUrl('');
        }

        return response === LoginProgressStatus.AUTHENTICATED;
      })
    );
  }
}
