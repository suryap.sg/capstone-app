import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material/material.module';
import { NavProfileComponent } from './components/nav-profile/nav-profile.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AccountStatusComponent } from './components/account-status/account-status.component';
import { LoginFailedComponent } from './components/login-failed/login-failed.component';

@NgModule({
  declarations: [NavProfileComponent, HomeComponent, AccountStatusComponent, LoginFailedComponent],
  imports: [CommonModule, HttpClientModule, RouterModule, FlexLayoutModule, MaterialModule],
  exports: [NavProfileComponent, HomeComponent, AccountStatusComponent]
})
export class CoreModule {}
