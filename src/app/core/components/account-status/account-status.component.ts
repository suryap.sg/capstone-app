import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { AccountService, LoginProgressStatus } from '../../account.service';

@Component({
  selector: 'app-account-status',
  templateUrl: './account-status.component.html',
  styleUrls: ['./account-status.component.scss']
})
export class AccountStatusComponent implements OnInit {
  private readonly destroy$ = new Subject();
  loginProgressStatusType = LoginProgressStatus;
  progressStatus: LoginProgressStatus = LoginProgressStatus.MSAL_AUTHENTICATING;
  constructor(private readonly accountService: AccountService, private readonly router: Router) {}

  ngOnInit(): void {
    this.accountService.loginProgress$.pipe(takeUntil(this.destroy$)).subscribe((status: LoginProgressStatus) => {
      this.progressStatus = status;

      if (status === LoginProgressStatus.AUTHENTICATED) {
        setTimeout(() => {
          this.router.navigateByUrl('/cdp-by-certs');
        }, 1000);
      }
    });
  }

  getSGAccessToken() {
    this.accountService.getSGAccessToken().pipe(take(1), takeUntil(this.destroy$)).subscribe();
  }
}
