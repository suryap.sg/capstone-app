import { Component, OnInit } from '@angular/core';
import { EMPTY, Subject } from 'rxjs';
import { catchError, take, takeUntil } from 'rxjs/operators';
import { AccountModel, AccountService } from '../../account.service';

@Component({
  selector: 'app-nav-profile',
  templateUrl: './nav-profile.component.html',
  styleUrls: ['./nav-profile.component.scss']
})
export class NavProfileComponent implements OnInit {
  destroy$ = new Subject();
  loading = true;
  account!: AccountModel;
  constructor(private readonly accountService: AccountService) {}

  ngOnInit(): void {
    this.loading = true;
    this.accountService
      .getAccount()
      .pipe(
        take(1),
        takeUntil(this.destroy$),
        catchError(_ => EMPTY)
      )
      .subscribe(account => {
        this.account = account;
        this.loading = false;
      });
  }
}
