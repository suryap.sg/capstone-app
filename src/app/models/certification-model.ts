export interface CertificationModel {
  id: string;
  name: string;
  description: string;
  price: string;
  approver: string;
  updated?: Date;
}
